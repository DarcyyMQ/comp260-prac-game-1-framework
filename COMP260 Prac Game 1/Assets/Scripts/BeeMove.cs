﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	private float speed;
	private float turnSpeed;

	private Transform target;
	public Vector2 heading = Vector2.right;

	public ParticleSystem explosionPrefab;

	// Use this for initialization
	void Start () {
		// find a player object to be the target
		PlayerMove player= FindObjectOfType<PlayerMove>();
		target = player.transform;

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		// set speed and turnSpeed randomly
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);
	}

	// Update is called once per frame
	void Update () {
		Vector2 direction = target.position - transform.position;

		float angle = turnSpeed * Time.deltaTime;

		if (direction.IsOnLeft (heading)) {
			//target on left rotate anticlockwise
			heading = heading.Rotate (angle);
		} else {
			//target on right rotate clockwise
			heading = heading.Rotate (-angle);
		}

		Vector2 velocity = heading * speed * Time.deltaTime;

		//Debug.Log ("Velocity: " + velocity);

		transform.Translate (velocity);
	}

	void OnDestroy(){
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}

	void OnDrawGizmos(){
		//draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);
	}
}
