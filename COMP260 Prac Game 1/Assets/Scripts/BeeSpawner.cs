﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;
	public float minBeePeriod = 1;
	public float maxBeePeriod = 5;
	private float beePeriod;
	private int nBeesSpawned = 0;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		// decrement the spawn timer
		beePeriod -= Time.deltaTime;

		if (beePeriod <= 0) {
			nBeesSpawned++;
			SpawnBee ();
			beePeriod = Random.Range (minBeePeriod, maxBeePeriod);
		}
	}

	void SpawnBee(){
		// instantiate a bee
		BeeMove bee = Instantiate(beePrefab);
		// attach to this object in the hierarchy
		bee.transform.parent = transform;
		// give the bee a name and number
		bee.gameObject.name = "Bee " + nBeesSpawned;

		// move the bee to a random position within the bounding rectangle
		float x = xMin + Random.value * width;
		float y = yMin + Random.value * height;
		bee.transform.position = new Vector2 (x, y);
	}

	public void DestroyBees(Vector2 centre, float radius){
		// destroy all bees within 'radius' of 'centre'
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy (child.gameObject);
			}
		}
	}
}
